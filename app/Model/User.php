<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Brand
 *
 * @author Shahzada Ali Saleem
 */
class User extends AppModel {
    /**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

    public $validate = array(
       
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Name should not be empty'
        )
    );
}

?>
